const MerkleTree = require('./modules/MerkleTree');

const data = [
  'abc',
  'def',
  'ghi',
  'jkl',
];

const customTree = new MerkleTree();

customTree.createTree(data);

console.log("\n== Data blocks : ", data);
console.log("\n== Merkle Root hash : ", customTree.root);
console.log("\n== Merkle tree depth : ", customTree.height());
console.log("\n== Merkle tree levels :");

customTree.displayTree();

console.log('\n== Wanna do some tests ? Execute `npm run test`\n\n');