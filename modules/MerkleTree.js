const { createHash } = require('crypto');

class MerkleTreeNode {
  constructor(dataString, leftChild = null, rightChild = null, parent = null) {
    // Create a leaf node or a non leaf node according to the dataString parameter
    if(!dataString && !rightChild){
      this.hash = leftChild.hash;
    }else{
      this.hash = createHash('sha256').update(
        (dataString) ? dataString : leftChild.hash + rightChild.hash
      ).digest('hex');
    }

    this.left = leftChild;
    this.right = rightChild;
    this.parent = parent;
  }
}

module.exports = class MerkleTree {

  constructor(){
    this.nodeList = [];
    this.rootNode = null;
    this.root = null;
  }

  /**
   * Get sibling'shashs according to a given level
   * @param index
   * @return {string[]}
   */
  level(index){
    let hashs = [];
    this.getSiblings(index).forEach(node => hashs.push(node.hash));
    return hashs;
  }

  /**
   * Get the height of the tree
   * @return {number}
   */
  height(){
    return this.maxDepth(this.rootNode);
  }

  /**
   * Create a Merkel tree according to a given array of data
   * @param dataArray
   */
  createTree(dataArray){
    this.nodeList = this.createLeaves(dataArray);
    this.createNodes(this.nodeList);
  }

  /**
   * Create the tree leaves according to a given array of data
   * @param dataArray
   * @return {MerkleTreeNode[]}
   */
  createLeaves(dataArray){
    return dataArray.map(value => new MerkleTreeNode(value));
  }

  /**
   * Create parent Merkle tree nodes according to a list of siblings
   * @param siblingsArray
   */
  createNodes(siblingsArray){
    let nodesCreated = [];

    for(let i = 0; i < siblingsArray.length; i++){
      if(siblingsArray[i]){
        let newNode = new MerkleTreeNode(null, siblingsArray[i]);

        if(siblingsArray[i+1]){
          newNode = new MerkleTreeNode(
            null,
            siblingsArray[i],
            siblingsArray[i+1]
          );
          siblingsArray[i].parent = newNode;
          siblingsArray[i+1].parent = newNode;
          i++;
        }else{
          siblingsArray[i].parent = newNode;
        }

        nodesCreated.push(newNode);
      }
    }

    this.nodeList = [...this.nodeList, ...nodesCreated];

    if(nodesCreated.length === 1){
      this.rootNode = nodesCreated[0];
      this.root = nodesCreated[0].hash;
    }else{
      this.createNodes(nodesCreated);
    }
  }

  /**
   * Display all hashs in the tree
   */
  displayTree(){
    for(let level = 1; level <= this.height(); level++){
      console.log("\nLevel ", level);
      console.log(this.level(level));
    }
  }

  /**
   * Retrieve siblings nodes according to a given level
   * @param level
   * @return {MerkleTreeNode[]}
   */
  getSiblings(level){
    return this.nodeList.filter(node => this.maxDepth(node) === this.height() - level + 1)
  }

  /**
   * Get the max depth of a tree according to a given node (root or not)
   * @param node
   * @return {number}
   */
  maxDepth(node){
    if(!node){
      return 0;
    }else{
      let left = this.maxDepth(node.left);
      let right = this.maxDepth(node.right);
      return Math.max(left, right) + 1;
    }
  }

};