const MerkleTree = require('../modules/MerkleTree');
const expect = require('chai').expect;
const { createHash } = require('crypto');

describe('MerkleTree class tests', () => {

  const data = [
    'abc',
    'def',
    'ghi',
    'jkl',
  ];


  describe('Tree creation', () => {

    const customTree = new MerkleTree();

    it('createLeaves() should create 4 leaves', () => {
      const leaves = customTree.createLeaves(data);
      expect(leaves.length).to.equal(4);
    });

    it('each leaves should have the right hash', () => {
      const leaves = customTree.createLeaves(data);
      const hashedData = data.map(el => createHash('sha256').update(el).digest('hex'));
      let allGood = (leaves.length === hashedData.length);
      for(let i in leaves){
        if(!hashedData.includes(leaves[i].hash)){
          allGood = false;
        }
      }
      expect(allGood).true;
    });

    it('createNodes() should create 3 nodes', () => {
      const leaves = customTree.createLeaves(data);
      customTree.createNodes(leaves);
      expect(customTree.nodeList.length).to.equal(3);
    });

    it('createTree() should have created 7 nodes in total', () => {
      customTree.createTree(data);
      expect(customTree.nodeList.length).to.equal(7);
    });

    it('root attribute should be right', () => {
      expect(customTree.root).to.equal('53804d89bf13aa7560f59e00e0c0e39cdcf0e1e02f5e7875bd5c6108dc2404f7');
    });


  });

  describe('Tree methods', () => {

    const customTree = new MerkleTree();

    customTree.createTree(data);

    it('height() should returns a height equal to 3', () => {
      expect(customTree.height()).to.equal(3);
    });

    it('level() should returns the right root hash (level 1)', () => {
      expect(customTree.level(1)[0]).to.equal('53804d89bf13aa7560f59e00e0c0e39cdcf0e1e02f5e7875bd5c6108dc2404f7');
    });

    it('getSiblings() should returns all nodes at the same level', () => {
      const siblingsHash = [
        'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad',
        'cb8379ac2098aa165029e3938a51da0bcecfc008fd6795f401178647f96c5b34',
        '50ae61e841fac4e8f9e40baf2ad36ec868922ea48368c18f9535e47db56dd7fb',
        '268f277c6d766d31334fda0f7a5533a185598d269e61c76a805870244828a5f1'
      ];

      const siblings = customTree.getSiblings(3);

      let allGood = (siblingsHash.length === siblings.length);

      for(let i in siblings){
        if(!siblingsHash.includes(siblings[i].hash)){
          allGood = false;
        }
      }

      expect(allGood).true;
    });

    it('maxDepth() should returns 3 from the root node', () => {
      expect(customTree.maxDepth(customTree.rootNode)).to.equal(3);
    });
  });
});